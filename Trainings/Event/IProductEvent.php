<?php
/**
 * Created by PhpStorm.
 * User: justdoit
 * Date: 31.01.19
 * Time: 0:23
 */

namespace Trainings;

use Trainings\Observer\IPublisher;

interface IProductEvent
{
    public function __construct(InfoProduct $p);
    public function getName();
    public function setDateStart(string $date_start);
    public function getDateStart(): string;
    public function getDateEnd(): string;
    public function setDateEnd(string $date_end);
    public function notifyManager(): IPublisher;

}