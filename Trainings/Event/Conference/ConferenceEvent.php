<?php
/**
 * Created by PhpStorm.
 * User: justdoit
 * Date: 31.01.19
 * Time: 0:44
 */

namespace Trainings;


use Trainings\Event\ProductEvent;
use Trainings\Observer\IPublisher;

class ConferenceEvent extends ProductEvent
{
    protected $product_type = "training";

    public function __construct(InfoProduct $p)
    {
        parent::__construct($p);

    }

    public function notifyingManager(): IPublisher
    {
        // TODO: Implement notifyingManager() method.
    }

    /**
     * Привязывает к событию (тренинг/конфа) конкретного publisher'a
     * @param IPublisher $p
     * @return mixed
     */
    protected function setPublisher(IPublisher $p)
    {
        // TODO: Implement setPublisher() method.
    }
}