<?php
/**
 * Created by PhpStorm.
 * User: justdoit
 * Date: 31.01.19
 * Time: 0:44
 */

namespace Trainings;

use Trainings\Event\ProductEvent;
use Trainings\Observer\IPublisher;
use Trainings\Observer\TrainingPublisher;

class TrainingEvent extends ProductEvent
{
    protected $product_type = "training";

    public function __construct(InfoProduct $p)
    {
        parent::__construct($p);
        // на данном этапе, со всеми тренингами будет работать TrainingPublisher
        $this->setPublisher(new TrainingPublisher($this));
    }

    /**
     * @return string
     */
    public function getProductType(): string
    {
        return $this->product_type;
    }

    /**
     * Привязывает к событию (тренинг/конфа) конкретного publisher'a
     * @param IPublisher $p
     * @return mixed
     */
    protected function setPublisher(IPublisher $p)
    {
        $this->notifying_manager = $p;

    }
}