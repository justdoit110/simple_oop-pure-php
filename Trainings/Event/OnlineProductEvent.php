<?php
/**
 * Created by PhpStorm.
 * User: justdoit
 * Date: 31.01.19
 * Time: 0:54
 */

namespace Trainings;


abstract class OnlineProductEvent implements IProductEvent
{
    private $product;
    public function __construct(InfoProduct $p)
    {
        $this->product = $p;
    }
}