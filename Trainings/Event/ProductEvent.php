<?php
/**
 * Created by PhpStorm.
 * User: justdoit
 * Date: 10.02.19
 * Time: 23:40
 */

namespace Trainings\Event;

use Trainings\InfoProduct;
use Trainings\IProductEvent;
use Trainings\Observer\IPublisher;

abstract class ProductEvent  implements IProductEvent
{
    /**
     * @var string
     */
    public $date_start;

    /**
     * @var string
     */
    public $date_end;

    /**
     * @var InfoProduct
     */
    private $product;

    /**
     * объект publisher, с которым будет работать
     * данное событие (тренинга/конференции)
     * @var IPublisher
     */
    protected $notifying_manager;

    public function __construct(InfoProduct $p)
    {
        $this->product = $p;
    }

    /**
     * Возвращат объект publisher'a события данного продукта
     * @return IPublisher
     */
    public function notifyManager(): IPublisher
    {
        return $this->notifying_manager;
    }

    /**
     * Привязывает к событию (тренинг/конфа) конкретного publisher'a
     * @param IPublisher $p
     * @return mixed
     */
    abstract protected function setPublisher( IPublisher $p);

    /**
     * Вернем название тренинга/конференции
     * в случае "преобразования" к строке
     */
    public function __toString(): string
    {
        return $this->getName();
    }

    /**
     * Имя берется из "продукта"
     * @return string
     */
    public function getName(): string
    {
        return $this->product->getName();
    }

    /**
     * @return string
     */
    public function getDateStart(): string
    {
        return $this->date_start;
    }

    /**
     * @param string $date_start
     */
    public function setDateStart(string $date_start)
    {
        $this->date_start = $date_start;
    }

    /**
     * @return string
     */
    public function getDateEnd(): string
    {
        return $this->date_end;
    }

    /**
     * @param string $date_end
     */
    public function setDateEnd(string $date_end)
    {
        $this->date_end = $date_end;
    }


}