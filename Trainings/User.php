<?php
/**
 * Created by PhpStorm.
 * User: justdoit
 * Date: 11.02.19
 * Time: 0:47
 */

namespace Trainings;


use Trainings\Observer\ISubscriber;

class User
{
    protected $name;

    protected $email;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return User
     */
    public function setName($name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
}