<?php
/**
 * Created by PhpStorm.
 * User: justdoit
 * Date: 31.01.19
 * Time: 0:42
 */

namespace Trainings;


class Training extends InfoProduct
{


    public function createEvent(): IProductEvent
    {
        return new TrainingEvent($this);
    }

    public function createOnlineEvent(): OnlineProductEvent
    {
        return new TrainingOnlineEvent($this);
    }
}