<?php
/**
 * Created by PhpStorm.
 * User: justdoit
 * Date: 31.01.19
 * Time: 0:23
 */

namespace Trainings;


interface IInfoProduct
{
    // фабричный метод
    function createEvent(): IProductEvent;
    function createOnlineEvent(): OnlineProductEvent;
}