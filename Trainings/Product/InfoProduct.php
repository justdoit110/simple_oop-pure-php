<?php
/**
 * Created by PhpStorm.
 * User: justdoit
 * Date: 31.01.19
 * Time: 0:24
 */

namespace Trainings;


abstract class InfoProduct implements IInfoProduct
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $description;

    abstract public function createEvent(): IProductEvent;
    abstract public function createOnlineEvent(): OnlineProductEvent;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return InfoProduct
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

}