<?php
/**
 * Created by PhpStorm.
 * User: justdoit
 * Date: 31.01.19
 * Time: 0:46
 */

namespace Trainings;


class Conference extends InfoProduct
{

    function createEvent(): IProductEvent
    {
        return new ConferenceEvent($this);
    }

    function createOnlineEvent(): OnlineProductEvent
    {
        return new ConferenceOnlineEvent($this);
    }
}