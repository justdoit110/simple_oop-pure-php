<?php
/**
 * Created by PhpStorm.
 * User: justdoit
 * Date: 11.02.19
 * Time: 1:24
 */

namespace Trainings\Observer;

use Trainings\IProductEvent;

interface ISubscriber
{
    public function update(string $event_type, IProductEvent $data);
}