<?php
/**
 * Created by PhpStorm.
 * User: justdoit
 * Date: 12.02.19
 * Time: 0:15
 */

namespace Trainings\Observer;


use Trainings\IProductEvent;

class TrainingPublisher extends EventPublisher
{
    const BONUS_VIP_INFO = 'bonus_info';

    /*
     * Все события объявляем константами и
     * кладем в массив
     */
    const EVENTS = [
        self::BEFORE_EVENT_START => 'one day before the start',
        self::AFTER_EVENT_END => 'after event ends',
        self::BONUS_VIP_INFO => 'blabla'
    ];

    public function __construct(IProductEvent $pe)
    {
        parent::__construct($pe);

    }

    public function eventBeforeStart(): void
    {
        echo PHP_EOL, ">>Уведомления до начала Тренинга<<", PHP_EOL;
        $this->notify(self::BEFORE_EVENT_START, $this->product_event);
    }

    public function eventEndsNotify(): void
    {
        echo PHP_EOL, ">>Уведомления после завершения Тренинга<<", PHP_EOL;
        $this->notify(self::AFTER_EVENT_END, $this->product_event);
        $this->eventBonusInfo();
    }

    public function eventBonusInfo(): void
    {
        echo PHP_EOL, ">>Уведомления С Бонусами Тренинга<<", PHP_EOL;
        $this->notify(self::BONUS_VIP_INFO, $this->product_event);

    }

}