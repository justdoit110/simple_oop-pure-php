<?php
/**
 * Created by PhpStorm.
 * User: justdoit
 * Date: 11.02.19
 * Time: 0:09
 */

namespace Trainings\Observer;


use Trainings\Client;
use Trainings\IProductEvent;

abstract class EventPublisher extends Publisher
{
    const BEFORE_EVENT_START = 'before_start';
    const AFTER_EVENT_END = 'after_end';
    const EVENTS = [];

    /**
     * Событие (тренинг/конференция) с которым будет работать publisher
     * @var IProductEvent
     */
    protected $product_event;

    public function __construct(IProductEvent $pe)
    {
        parent::__construct();
        $this->initEventList();
        $this->product_event = $pe;
    }

    /**
     * Инициализирует ассоциативный массив
     * где ключом является тип события (из EVENTS)
     * а значение - объект SplObjectStorage в котором будут
     * хранится клиенты/подписчики
     */
    protected function initEventList(): void
    {
        // Позднее статическое связывание "static::"
        // здесь мы получим EVENTS вызываемого класса
        // если "self::" то всегда будет использован EVENTS этого класса
        foreach (array_keys(static::EVENTS) as $event_type ) {
            $this->subscribers[$event_type] = new \SplObjectStorage();
        }
    }

    /**
     * Отправляет уведомления подписчикам события $event_type
     * @param string $event_type
     * @param $data
     */
    protected function notify(string $event_type, $data)
    {
        /**
         * @var $subscriber Client
         */
        foreach ($this->subscribers[$event_type] as $subscriber ) {
            // на всякий случай обернем в try catch
            // что бы не прервалась "рассылка" если вдруг что-то "не так"
            try {
                $subscriber->update($event_type, $data);
            }catch (\Exception $e) {
                echo "ERROR: on notifying client: {$subscriber->getEmail()}, event_type: {$event_type}\n Message: {$e->getMessage()}";
            }
        }
    }
}