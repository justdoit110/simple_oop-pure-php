<?php
/**
 * Created by PhpStorm.
 * User: justdoit
 * Date: 12.02.19
 * Time: 0:29
 */

namespace Trainings\Observer;


interface IPublisher
{
    function subscribeOn(string $event_type, ISubscriber $s);
    function unSubscribeOn(string $event_type, ISubscriber $s): void;

}