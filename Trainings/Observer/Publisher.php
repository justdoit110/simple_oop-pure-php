<?php
/**
 * Created by PhpStorm.
 * User: justdoit
 * Date: 11.02.19
 * Time: 0:04
 */

namespace Trainings\Observer;


class Publisher implements IPublisher
{
    /**
     * Массив доступных событий (key string) и подписанных на эти события клиентов (value SplObjectStorage)
     * @var array | string => \SplObjectStorage
     */
    protected $subscribers;

    public function __construct()
    {
    }

    /**
     * Подписка клиента на события
     * @param string $event_type
     * @param ISubscriber $s
     */
    public function subscribeOn(string $event_type, ISubscriber $s): void
    {
        // проверим что такой $event_type существует
        if(!isset($this->subscribers[$event_type])) return;

        $this->subscribers[$event_type]->attach($s);
        echo "Клиент {$s->getName()} подписался на уведомления '{$event_type}'", PHP_EOL;
    }

    /**
     * Отписка клиента от конкретного события
     * @param string $event_type
     * @param ISubscriber $s
     */
    public function unSubscribeOn(string $event_type, ISubscriber $s): void
    {
        // проверим что такой $event_type существует
        if(!isset($this->subscribers[$event_type])) return;

        $this->subscribers[$event_type]->detach($s);
    }


}