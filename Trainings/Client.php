<?php
/**
 * Created by PhpStorm.
 * User: justdoit
 * Date: 11.02.19
 * Time: 0:48
 */

namespace Trainings;

use Trainings\Observer\ISubscriber;
use Trainings\Observer\TrainingPublisher;

class Client extends User implements ISubscriber
{

    /**
     * Отправляет уведомление клиенту события (тренинга/конфы)
     * в зависимости от типа события (event_type)
     * @param string $event_type
     * @param IProductEvent $data
     * @throws \Exception
     */
    public function update(string $event_type, IProductEvent $data): void
    {
        switch ($event_type) {
            case TrainingPublisher::BEFORE_EVENT_START:
                $this->notifyBeforeEventStart($data);
                break;
            case TrainingPublisher::AFTER_EVENT_END:
                $this->notifyAfterEventEnds($data);
                break;
            case TrainingPublisher::BONUS_VIP_INFO:
                $this->notifyBonusInfo($data);
                break;
            default:
        }
    }

    private function notifyBeforeEventStart(IProductEvent $data): void
    {
        echo "Уведомление для {$this->getEmail()} до начала события {$data->getName()}",PHP_EOL;
    }

    private function notifyAfterEventEnds(IProductEvent $data): void
    {
        echo "Уведомление для {$this->getEmail()} после завершения события {$data->getName()}",PHP_EOL;
    }

    private function notifyBonusInfo(IProductEvent $data): void
    {
        echo "Уведомление для {$this->getEmail()} бонусная рассылка, события {$data->getName()}",PHP_EOL;
    }
}