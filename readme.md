#### Небольшой ООП пример.
(Фабричный метод, Наблюдатель, статическое связывание)

Представим что существует некоторый Инфопродукт `(Product\IInfoProduct)`, это может 
быть Тренинг `(Product\Training)` или Конференция `(Product\Conference)` они чем-то отличаются.

Конкретный продукт может иметь даты когда он будет проходить, назовем их События `(Event\IProductEvent)`.
Экземпляр продукта `Training` или `Conference` реализует фабричный метод `createEvent()`
и создает соответственно либо `Event\Trainig\TrainigEvent` либо `Event\Conference\ConferenceEvent`.

Также имеем Клиента (участника тренинга/конфы) реализующего простой вариант `Subscriber`. `Publisher` вынесен в отдельный 
объект который посредством композиции может работать с Тренингами `(Observer\TrainingPublisher)`
 или с Конференциями (метод `setPublisher` в классе продукта). `TrainingPublisher` 
 реализует типы событий на которые может подписаться Клиент.
  
 Пример позднего статического связывания описан в 
 `Observer\EventPublisher::initEventList` 
 
```bash
 composer dump-autoload
 php index.php
```
 