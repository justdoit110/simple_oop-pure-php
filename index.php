<?php
// Autoload files using the Composer autoloader.
use Trainings\Observer\TrainingPublisher;

require_once __DIR__ . '/vendor/autoload.php';

$training = new \Trainings\Training();
$training->setName("Тренинг - 'В Чем Смысл ООПЭ'");

$event = $training->createEvent();
$event->setDateStart("Скоро");

$client1 = new \Trainings\Client();
$client1->setName("Elon Musk")->setEmail("elon@musk.com");

$client2 = new \Trainings\Client();
$client2->setName("Mr. Freeman")->setEmail("free@man.com");

$event->notifyManager()->subscribeOn(TrainingPublisher::BEFORE_EVENT_START, $client1);
$event->notifyManager()->subscribeOn(TrainingPublisher::BEFORE_EVENT_START, $client2);
$event->notifyManager()->subscribeOn(TrainingPublisher::AFTER_EVENT_END, $client2);
$event->notifyManager()->subscribeOn(TrainingPublisher::BONUS_VIP_INFO, $client2);
// эта продписка будет проигнорирована т.к. нет такого event_type
$event->notifyManager()->subscribeOn("wrong", $client1);
$event->notifyManager()->eventBeforeStart();
$event->notifyManager()->eventEndsNotify();